\select@language {ngerman}
\contentsline {chapter}{Danksagung}{iii}{chapter*.1}
\contentsline {chapter}{Abstract}{iv}{chapter*.2}
\contentsline {chapter}{Zusammenfassung}{v}{chapter*.3}
\contentsline {chapter}{Abbildungsverzeichnis}{x}{chapter*.5}
\contentsline {chapter}{Tabellenverzeichnis}{xi}{chapter*.6}
\contentsline {chapter}{Abk\"urzungsverzeichnis}{xii}{chapter*.7}
\contentsline {chapter}{\numberline {1}Einleitung}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Genderhinweis}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Problemstellung}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Zielsetzung und Motivation}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Aufbau der Arbeit}{2}{section.1.4}
\contentsline {section}{\numberline {1.5}Vorgangsweise}{3}{section.1.5}
\contentsline {chapter}{\numberline {2}Besser leben im Alter durch Technik}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}CareTech}{6}{section.2.1}
\contentsline {section}{\numberline {2.2}Portal}{7}{section.2.2}
\contentsline {section}{\numberline {2.3}Ablauf der Beratung und Netzwerkaktivit\"at}{8}{section.2.3}
\contentsline {chapter}{\numberline {3}Grundlagen}{10}{chapter.3}
\contentsline {section}{\numberline {3.1}Staats- und Verwaltungsaufbau von Deutschland}{10}{section.3.1}
\contentsline {section}{\numberline {3.2}Evaluation}{11}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Ablauf einer Evaluation}{12}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Methoden}{15}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Beratung}{16}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Definition Beratung}{16}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Aufbau einer Beratung}{17}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Beratungserfolg}{17}{subsection.3.3.3}
\contentsline {subsubsection}{\numberline {3.3.3.1}Gr\"unde f\"ur eine Messung des Beratungserfolges}{17}{subsubsection.3.3.3.1}
\contentsline {section}{\numberline {3.4}Kundenorientierung}{18}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Messung der Zufriedenheit}{18}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}\"Offentlichkeitsarbeit}{22}{subsection.3.4.2}
\contentsline {chapter}{\numberline {4}Konzeptarbeiten}{24}{chapter.4}
\contentsline {section}{\numberline {4.1}Ist-Zustand}{25}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Auswertung des Fragebogens}{32}{subsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.1.1}Regionale Ansiedelung der KBS}{32}{subsubsection.4.1.1.1}
\contentsline {subsubsection}{\numberline {4.1.1.2}Aktuelle Situation}{33}{subsubsection.4.1.1.2}
\contentsline {subsubsection}{\numberline {4.1.1.3}Berater}{34}{subsubsection.4.1.1.3}
\contentsline {subsubsection}{\numberline {4.1.1.4}Zielgruppe und Angebot}{35}{subsubsection.4.1.1.4}
\contentsline {subsubsection}{\numberline {4.1.1.5}Kooperationen}{38}{subsubsection.4.1.1.5}
\contentsline {subsubsection}{\numberline {4.1.1.6}Finanzierung}{39}{subsubsection.4.1.1.6}
\contentsline {section}{\numberline {4.2}Erstellung der Hypothesen}{40}{section.4.2}
\contentsline {section}{\numberline {4.3}Ablauf der Datenerhebung f\"ur die Evaluation}{43}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Leitfaden f\"ur die Evaluation}{44}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Kontinuierlicher Verbesserungsprozess}{47}{subsection.4.3.2}
\contentsline {chapter}{\numberline {5}Auswertung der Ergebnisse}{48}{chapter.5}
\contentsline {section}{\numberline {5.1}Befragung der Beratenden}{48}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}CareTech}{48}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Andere Dokumentationsarten}{49}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Wegweiser Portal}{51}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}Spezifische Webseite der Beratungsstelle}{53}{subsection.5.1.4}
\contentsline {subsection}{\numberline {5.1.5}Kooperationspartner und Dienstleister}{54}{subsection.5.1.5}
\contentsline {section}{\numberline {5.2}Dokumentation}{58}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Alter}{58}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Erbrachte Leistungen}{59}{subsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.2.1}Beratungst\"atigkeiten}{61}{subsubsection.5.2.2.1}
\contentsline {subsubsection}{\numberline {5.2.2.2}\"Offentlichkeitsarbeit}{61}{subsubsection.5.2.2.2}
\contentsline {subsubsection}{\numberline {5.2.2.3}Andere T\"atigkeiten}{62}{subsubsection.5.2.2.3}
\contentsline {subsection}{\numberline {5.2.3}Folgetermine}{62}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Themen}{63}{subsection.5.2.4}
\contentsline {subsection}{\numberline {5.2.5}Weiterempfehlungen}{64}{subsection.5.2.5}
\contentsline {subsection}{\numberline {5.2.6}Kooperationen}{66}{subsection.5.2.6}
\contentsline {subsection}{\numberline {5.2.7}\"Offnungszeiten/Sprechzeiten}{68}{subsection.5.2.7}
\contentsline {subsection}{\numberline {5.2.8}Alter und Themen}{70}{subsection.5.2.8}
\contentsline {subsection}{\numberline {5.2.9}Empfehlungen}{71}{subsection.5.2.9}
\contentsline {subsection}{\numberline {5.2.10}Schulungen/Vortr\"age}{72}{subsection.5.2.10}
\contentsline {subsection}{\numberline {5.2.11}Demonstrationsumgebung}{73}{subsection.5.2.11}
\contentsline {section}{\numberline {5.3}Befragung zur Besucherzufriedenheit}{73}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Beratung Allgemein}{74}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Erreichbarkeit,Zust\"andigkeit und Kooperation}{75}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Produkte}{75}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Dienstleistungen}{77}{subsection.5.3.4}
\contentsline {chapter}{\numberline {6}Fazit zum Aufbau und Betrieb einer Beratungsstelle}{78}{chapter.6}
\contentsline {section}{\numberline {6.1}Aufbau}{78}{section.6.1}
\contentsline {section}{\numberline {6.2}Betrieb}{79}{section.6.2}
\contentsline {chapter}{\numberline {7}Diskussion und Ausblick}{81}{chapter.7}
\contentsline {section}{\numberline {7.1}Diskussion}{81}{section.7.1}
\contentsline {section}{\numberline {7.2}Ausblick}{81}{section.7.2}
\contentsline {chapter}{Literaturverzeichnis}{83}{chapter*.43}
\contentsline {chapter}{Internetquellen}{86}{chapter*.44}
\contentsline {chapter}{\numberline {A}Dokumente}{88}{appendix.A}
\contentsline {section}{\numberline {A.1}Reihenfolge der Dokumente}{88}{section.A.1}
\contentsline {chapter}{Eidesstattliche Erkl\"{a}rung}{111}{appendix*.45}
